unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    LBox_carros: TListBox;
    Ed_modelo: TEdit;
    Ed_ano: TEdit;
    Ed_marca: TEdit;
    Bt_inserir: TButton;
    Bt_deletar: TButton;
    Bt_atualizar: TButton;
    bt_salvar: TButton;
    Bt_carregar: TButton;
    procedure Bt_inserirClick(Sender: TObject);
    procedure Bt_deletarClick(Sender: TObject);
    procedure Bt_atualizarClick(Sender: TObject);
    procedure bt_salvarClick(Sender: TObject);
    procedure Bt_carregarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
   TCarros = class(TObject)
    Modelo:string;
    Ano:integer;
    Marca:string;
  end;
var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Bt_atualizarClick(Sender: TObject);
var carro2:TCarros;
var modelo, ano, marca: string;
begin
  modelo:=ed_modelo.Text;
   ano:=ed_ano.Text;
  marca:= ed_marca.Text;

  if (modelo.Length<>0) and (ano.Length<>0) and (marca.Length<>0)  then
  begin
    carro2 :=lbox_carros.Items.Objects[lbox_carros.ItemIndex] as Tcarros;
    carro2.modelo := modelo;
    carro2.ano := strtoint(ano);
   carro2.marca:= marca;
    lbox_carros.Items[lbox_carros.ItemIndex]:= ed_modelo.Text ;
  end
  else
  begin
    ShowMessage('Preencha todos os campos!');
  end;


end;



procedure TForm1.Bt_carregarClick(Sender: TObject);
   var arq : TextFile;
   var carro3:Tcarros;
begin
    AssignFile(arq, 'tabela_carros.txt');
     reset (arq);
     carro3:= tcarros.Create();
        while not Eof (arq) do
        BEGIN
               ReadLn(arq, carro3.modelo );
               ReadLn(arq, carro3.ano );
               ReadLn(arq, carro3.marca );
            LBox_carros.Items.AddObject(carro3.Modelo,carro3);
        end;
 END;

procedure TForm1.Bt_deletarClick(Sender: TObject);
begin
      lbox_carros.DeleteSelected;
end;

procedure TForm1.Bt_inserirClick(Sender: TObject);
var carro:TCarros;
var modelo, ano, marca: string;
begin
  modelo:=ed_modelo.Text;
   ano:=ed_ano.Text;
  marca:= ed_marca.Text;

  if (modelo.Length<>0) and (ano.Length<>0) and (marca.Length<>0)  then
  begin
    carro := Tcarros.Create;
    carro.modelo := modelo;
    carro.ano := strtoint(ano);
   carro.marca:= marca;


    Lbox_carros.Items.AddObject(carro.modelo, carro);
  end
  else
  begin
    ShowMessage('Preencha todos os campos!');
  end;

end;



procedure TForm1.bt_salvarClick(Sender: TObject);
var arq : TextFile;
var carro2:Tcarros;
var i: integer;
begin

      AssignFile(arq, 'tabela_carros.txt');
      Rewrite(arq);
      for i:=1 to Pred(lbox_carros.Items.Count) do
      begin
        carro2:= lbox_carros.Items.Objects[i] as Tcarros;
        writeln(arq,'registro ', i,'=', carro2.Modelo);
        writeln(arq,'registro ',i,'=',inttostr(carro2.ano));
        writeln(arq,'registro ',i,'=',carro2.Marca);
      end;

      CloseFile(arq);
    end;

end.
