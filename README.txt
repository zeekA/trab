Aluno: Ezequiel Campos
Turma : 3-53

(1) As estruturas de programa��o das linguagens permiem criar logicas para o fluxo dos dados de nossos softwares. Assinale a alternativa que contem uma estutura de programa��o usada pela linguagem Object-Pascal que permite iterar indefinidamente sobre um mesmo conjunto de instru�oes:

D) WHILE : pois ela � usada quando precisamos de um estrutura de repeti��o.

(2) As estruturas de dados presentes nas linguagens de programa��o s�o necessarias para que a aplica��o possa entender e armazenarcorretamente os dados que pretendemos utilizar. Assinale a alternativa que contem, respectivamente, uma estrutura capaz de armazenar apenas numeros inteiros positivos e negativos e uma estrutura que armazena apenas dois valores:

(3) Varias linguagens de programa��o permitem a cria��o de enumeradores. Este tipo de estutura de dados permite criar uma variavel que armazena um valor pre-definido pelo proprio programador. Utilizando como referencia a linguagem Object-Pascal, assinale a alternativa que representa a cria��o de um enumerador para os dias de semana:

(4) Existe uma estrutura definida pelo Object-Pascal capaz de agrupar itens de dados de diferentes tipos (ao contrario do array, que armazena varios itens de mesmo tipo). Assinale a alternativa que representa o nome desta estrutura:

(5) O Git � um sistema de controle de versoes desenvolvido por Linues Torvalds e Junio Hamano que facilita o processo de desenvolvimento de software ao ser usado para registrar o historico de edi�oes dos arquivos-fonte. Assinale a alternativa que n�o representa uma fun��o do Git:

(6) Para organizar a area de desenvolvimento, o Git implementa diversas areas com diferentes caracteristicas dentro de um projeto. Dessa forma, o Git minimiza altera�oes desastrosas que podem comprometer a integridade do codigo. Assinale a alternativa que representa o nome da area do Git onde ficam armazenados os arquivos que estao prontos para serem preservados permanentemento no repositorio local (porem ainda nao foram) :

(7) Ao realizar altera�oes no codigo, o Git nao preserva automaticamente as altera�oes efetuadas em seu repositorio. Antes de mais nada, � necessario indicar ao versionador quais s�o as modifica�oes que pretendemos preservar em uma nova vers�o. Assinale a alternativa que preserva APENAS as altera�oes realizar no arquivo stark.php (levando em considera��o que o arquivo do mesmo projeto vingadores.php tambem possui altera�oes):

A) git add stark.php: serve para adionar qual arquivo fizemos alguma altera��o.

(8) Quando h� modifica�oes listadas no Index do repositorio Git, podemos preservar permanentemente estas alte�oes em um processo chamado de commit. Assinale a alternativa que melhor descreve o comportamente do comando git commit:

B) Cria-se um identificar unico para o commit e as modifca�oes s�o preservadas no repositorio local : ent�o esse comando serve para nois identificar qual altera��o nois fez no arquivo e atualizar ele.

(9) O repositorio remoto � uma defini��o do Git para uma copia remota do repositorio local de um determinado projeito. No entanto, estes dois tipos de repositrio podem conter diferen�as entre si, que necessitam da atualiza��o do programador. Assinale a alternativa que representa, respectivamente, o comando utilizado para tualizar o repositorio local (com o conteudo remoto) e o comando utilizado para tualizar o repositorio remoto (com o conteudo local)

(10) Os gerenciadores de repositorio baseados em Git (como o GitLab) s�o responsaveis por permitir que o desenvolvedores armazenam remotamente seus repostorios e oferecem ferramentas para a constru��o de software de maneira colaborativa. Assinale a alternativa que apresenta o comando utilizado para realizar a copia de um repositorio para a sua maquina, levando em considera��o:

Servidor: gitlab.com
Usuario: rvenson
Repositorio: prova01

D) git clone https://gitlab.com/rvenson/prova01 : essa era so olhar o nome do usuario e saber qual era o repositorio, e claro saber qual comando usar que no caso era o git clone.